package com.company;

public class Word implements Comparable<Word>{
    private String word;
    private int counter;

    Word(String word) {
        this.word = word;
        this.counter = 0;
    }

    public void increaseCounter() {
        this.counter += 1;
    }

    public int getCounter() {
        return this.counter;
    }

    @Override
    public int compareTo(Word o) {
        return this.getCounter() - o.getCounter();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.getClass().equals(this.getClass()) && this.word.equals(((Word) obj).word);
    }

    @Override
    public String toString() {
        return this.word;
    }
}
