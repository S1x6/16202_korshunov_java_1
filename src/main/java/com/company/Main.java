package com.company;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        WordsCounter counter = new WordsCounter();
        try {
            counter.countWordsInFile("words.txt");
            counter.printStatisticToFile("out.txt");
        } catch (IOException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }
}
