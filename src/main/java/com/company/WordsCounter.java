package com.company;

import java.io.*;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class WordsCounter {
    private TreeMap<String,Word> wordMap;
    private int wordsInTotal;
    WordsCounter() {
        wordMap = new TreeMap<>();
        this.wordsInTotal = 0;
    }

    public void countWordsInFile(String fileName) throws IOException{
        Reader reader = new InputStreamReader(new FileInputStream(fileName), "UTF-8");
        this.wordsInTotal = 0;
        int char_i = reader.read();
        while (char_i != -1) {
            StringBuilder sb = new StringBuilder();
            char c = (char)char_i;
            while (Character.isLetterOrDigit(c)) {
                sb.append(String.valueOf(c));
                char_i = reader.read();
                c = (char)char_i;
            }
            char_i = reader.read();
            Word word = new Word(sb.toString());
            if (wordMap.containsKey(word.toString())){
                wordMap.get(word.toString()).increaseCounter();
            } else {
                word.increaseCounter();
                wordMap.put(word.toString(),word);
            }
            ++wordsInTotal;
        }
    }

    public void printStatisticToFile(String fileName) throws IOException{
        FileWriter writer = new FileWriter(fileName);
        for(Map.Entry<String, Word> item : reverseSortByValues(wordMap).entrySet()){
            writer.write(item.getValue().toString() + ", " + item.getValue().getCounter() + ", " + (((float) item.getValue().getCounter()) / this.wordsInTotal * 100) + "\r\n");
        }
        writer.flush();
    }

    private static <K, V extends Comparable<V>> Map<K, V> reverseSortByValues(final Map<K, V> map) {
        Comparator<K> valueComparator = (k1, k2) -> {
            int compare = map.get(k1).compareTo(map.get(k2));
            if (compare == 0)
                return 1;
            else
                return -compare;
        };

        Map<K, V> sortedByValues =
                new TreeMap<>(valueComparator);
        sortedByValues.putAll(map);
        return sortedByValues;
    }
}
